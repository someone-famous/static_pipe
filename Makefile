
build: components index.js
	@component build --dev

components: component.json
	@component install --dev

test:
	./node_modules/.bin/mocha --reporter spec

.PHONY: test
