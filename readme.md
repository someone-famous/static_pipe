Initially start app`

`` bash
    npm install
``

On MacOS or Linux, run the app with this command:

`` bash
    DEBUG=static_pipe:* npm start
``

On Windows, use this command:

`` bash
    set DEBUG=static_pipe::* & npm start`
``
