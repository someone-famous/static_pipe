var express = require('express');
var router = express.Router();

/* POST "hello world" from pubsub service. */
router.post('/', function(request, response, next) {
    console.log(request.body);      // your JSON
    response.send('Hello World as a POST!');    // echo the result back
    // res.render('Hello World!');
});

router.get('/', function(request, response, next) {
    console.log(request.body);      // your JSON
    response.send('Hello World as a GET!');    // echo the result back
    // res.render('Hello World!');
});

module.exports = router;